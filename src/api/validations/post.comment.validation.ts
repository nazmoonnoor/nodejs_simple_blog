import Joi from "joi";
import { PostComment } from "../interfaces";

export const Validator = {
	postComment: {
		create: Joi.object<PostComment>({
			post_id: Joi.number().required(),
			title: Joi.string().required(),
			comment: Joi.string().required(),
		}),
		update: Joi.object<PostComment>({
			title: Joi.string().optional(),
			comment: Joi.string().optional(),
		}),
	},
};
