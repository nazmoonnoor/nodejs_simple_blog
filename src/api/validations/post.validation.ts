import Joi from "joi";
import { Post } from "../interfaces";

export const Validator = {
	post: {
		create: Joi.object<Post>({
			title: Joi.string().required(),
			content: Joi.string().required(),
		}),
		update: Joi.object<Post>({
			title: Joi.string().optional(),
			content: Joi.string().optional(),
		}),
	},
};
