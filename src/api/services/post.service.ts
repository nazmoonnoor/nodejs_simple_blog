import PostRepository from "../../db/repository/post.repository";
import { GetAllPostsFilters } from "../../db/repository/types";
import { PostInput, PostOutput } from "../../db/models/post";
import { ApiInternalError, BadRequestError, NotFoundError } from "../../utils/errorHandler";

export default class PostService {
	constructor(private readonly postRepository: PostRepository) {
		this.postRepository = postRepository;
	}

	create = async (payload: PostInput): Promise<PostOutput | null> => {
		const created = await this.postRepository.create(payload);
		if (!created) throw new ApiInternalError(`Post could not be saved.`);

		return created;
	};

	update = async (id: number, payload: Partial<PostInput>): Promise<PostOutput | null> => {
		if (!id) throw new BadRequestError(`Argument exception. Post Id not provided.`);

		const updated = await this.postRepository.update(id, payload);
		if (!updated) throw new BadRequestError(`Post with id = ${id} not found.`);

		return updated;
	};

	getById = async (id: number): Promise<PostOutput | null> => {
		if (!id) throw new BadRequestError(`Argument exception. Post Id not provided.`);

		const post = await this.postRepository.getById(id);
		if (!post) throw new BadRequestError(`Post with id=${id} not found.`);

		return post;
	};

	deleteById = async (id: number): Promise<boolean> => {
		return this.postRepository.deleteById(id);
	};

	getAll = async (filters: GetAllPostsFilters): Promise<PostOutput[] | undefined> => {
		const posts = await this.postRepository.getAll(filters);
		if (!posts) throw new NotFoundError(`Not found.`);
		return posts;
	};
}
