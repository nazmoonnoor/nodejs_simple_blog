import UserRepository from "../../db/repository/user.repository";
import { GetAllUsersFilters } from "../../db/repository/types";
import { UserInput, UserOutput } from "../../db/models/user";
import { ApiInternalError, BadRequestError, NotFoundError } from "../../utils/errorHandler";

export default class UserService {
	constructor(private readonly userRepository: UserRepository) {
		this.userRepository = userRepository;
	}

	create = async (payload: UserInput): Promise<UserOutput | null> => {
		const created = await this.userRepository.create(payload);
		if (!created) throw new ApiInternalError("");

		return created;
	};

	update = async (id: number, payload: Partial<UserInput>): Promise<UserOutput | null> => {
		if (!id) throw new BadRequestError(`Argument exception. User Id not provided.`);

		const updated = await this.userRepository.update(id, payload);
		if (!updated) throw new BadRequestError(`User with id = ${id} not found.`);

		return updated;
	};

	getById = async (id: number): Promise<UserOutput | null> => {
		if (!id) throw new BadRequestError(`Argument exception. User Id not provided.`);

		const user = await this.userRepository.getById(id);
		if (!user) throw new BadRequestError(`User with id=${id} not found.`);

		return user;
	};

	deleteById = async (id: number): Promise<boolean> => {
		return this.userRepository.deleteById(id);
	};

	getAll = async (filters: GetAllUsersFilters): Promise<UserOutput[] | undefined> => {
		const users = await this.userRepository.getAll(filters);
		if (!users) throw new NotFoundError(`Not found.`);
		return users;
	};
}
