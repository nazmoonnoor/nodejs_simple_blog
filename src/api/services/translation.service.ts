import dotenv from "dotenv";
import axios from "axios";
import { PostInput, PostOutput } from "../../db/models/post";
import PostRepository from "../../db/repository/post.repository";
import { ApiInternalError } from "../../utils/errorHandler";
import { TranslateDTO } from "../dto/translate.dto";
import PostService from "./post.service";

dotenv.config();

export default class TranslationService {
	postService: PostService;

	constructor() {
		this.postService = new PostService(new PostRepository());
	}

	getTranslation = async (payload: TranslateDTO): Promise<string> => {
		const encodedParams = new URLSearchParams();
		encodedParams.append("q", payload.text);
		encodedParams.append("source", payload.sourceLang || "en");
		encodedParams.append("target", payload.targetLang || "de");

		const options = {
			method: "POST",
			url: "https://google-translate1.p.rapidapi.com/language/translate/v2",
			headers: {
				"content-type": "application/x-www-form-urlencoded",
				"Accept-Encoding": "application/gzip",
				"X-RapidAPI-Key": process.env.RAPIDAPI_KEY || "",
				"X-RapidAPI-Host": process.env.RAPIDAPI_HOST || "",
			},
			data: encodedParams,
		};

		try {
			const response: any = await axios.request(options);
			console.log(response.data);
			const translatedText = response?.data?.data?.translations?.[0]?.translatedText;
			return translatedText || "";
		} catch (error) {
			throw new ApiInternalError("Translation was failed.");
		}
	};

	saveTranslation = async (payload: PostInput): Promise<PostOutput | null> => {
		const created = this.postService.create(payload);
		if (!created) throw new ApiInternalError(`Post could not be saved.`);

		return created;
	};
}
