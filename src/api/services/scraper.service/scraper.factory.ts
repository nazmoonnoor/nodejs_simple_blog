import ScraperBase from "./scraper.base";
import DevToScraper from "./scraper.dev.to";
import MozillaScraper from "./scraper.mozilla";

export default class ScraperFactory {
	createScraperObj(url: string): ScraperBase | null {
		if (url.includes("dev.to")) {
			return new DevToScraper();
		}
		if (url.includes("blog.mozilla.org")) {
			return new MozillaScraper();
		}

		return null;
	}
}
