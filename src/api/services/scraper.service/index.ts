import { Browser } from "puppeteer";
import PostRepository from "../../../db/repository/post.repository";
import logger from "../../../utils/logger";
import { CreatePostDTO } from "../../dto/post.dto";
import PostService from "../post.service";
import ScraperFactory from "./scraper.factory";

export default class ScraperService {
	postService: PostService;

	constructor(private readonly browser: Browser) {
		this.browser = browser;
		this.postService = new PostService(new PostRepository());
	}

	/** Scrap data from page, instanciate based on url pattern
	 * @param  {any} opt
	 * @returns Promise
	 */
	scrapPage = async (opt: any): Promise<CreatePostDTO | null> => {
		const page = await this.browser.newPage();
		logger.info(`Navigating to ${opt.url}...`);
		await page.goto(opt.url, {
			waitUntil: "load",
			timeout: 0,
		});

		const html = await page.evaluate(() => {
			return document.documentElement.innerHTML;
		});

		const factory: ScraperFactory = new ScraperFactory();
		const scraper = factory.createScraperObj(opt.url);
		if (!scraper) return null;

		const post = await scraper.scrap(html);

		// Save to db
		await this.postService.create({
			title: post.title,
			content: post.content,
		});

		return post;
	};
}
