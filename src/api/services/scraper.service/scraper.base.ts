import { Post } from "./post.model";

export default abstract class Scraper {
	abstract scrap(content: string): Promise<Post>;
}
