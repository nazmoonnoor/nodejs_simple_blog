export type Post = {
	title: string;
	content: string;
	description?: string;
	publishedAt?: Date;
};
