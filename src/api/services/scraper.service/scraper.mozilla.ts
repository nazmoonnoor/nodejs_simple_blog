import cheerio from "cheerio";
import { Post } from "./post.model";
import Scraper from "./scraper.base";
import logger from "../../../utils/logger";

export default class MozillaScraper extends Scraper {
	scrap = async (content: string): Promise<Post> => {
		const $ = cheerio.load(content);

		const headerText = $("h1.ft-c-single-post__title").text().trim();
		const articleElement = $(".ft-c-single-post__body").html();

		logger.info(headerText);
		return {
			title: headerText,
			content: articleElement || "",
		};
	};
}
