import cheerio from "cheerio";
import { Post } from "./post.model";
import Scraper from "./scraper.base";
import logger from "../../../utils/logger";

export default class DevToScraper extends Scraper {
	scrap = async (content: string): Promise<Post> => {
		const $ = cheerio.load(content);

		const headerText = $(".crayons-article__header__meta > h1").text().trim();
		const articleElement = $(".crayons-article__main").html();

		logger.info(headerText);
		return {
			title: headerText,
			content: articleElement || "",
		};
	};
}
