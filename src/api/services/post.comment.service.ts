import PostCommentRepository from "../../db/repository/post.comment.repository";
import { GetAllPostCommentsFilters } from "../../db/repository/types";
import { PostCommentInput, PostCommentOutput } from "../../db/models/post.comment";
import { ApiInternalError, BadRequestError, NotFoundError } from "../../utils/errorHandler";

export default class PostCommentService {
	constructor(private readonly postCommentRepository: PostCommentRepository) {
		this.postCommentRepository = postCommentRepository;
	}

	create = async (payload: PostCommentInput): Promise<PostCommentOutput | null> => {
		const created = await this.postCommentRepository.create(payload);
		if (!created) throw new ApiInternalError("");

		return created;
	};

	update = async (
		id: number,
		payload: Partial<PostCommentInput>
	): Promise<PostCommentOutput | null> => {
		if (!id) throw new BadRequestError(`Argument exception. PostComment Id not provided.`);

		const updated = await this.postCommentRepository.update(id, payload);
		if (!updated) throw new BadRequestError(`PostComment with id = ${id} not found.`);

		return updated;
	};

	getById = async (id: number): Promise<PostCommentOutput | null> => {
		if (!id) throw new BadRequestError(`Argument exception. PostComment Id not provided.`);

		const postComment = await this.postCommentRepository.getById(id);
		if (!postComment) throw new BadRequestError(`PostComment with id=${id} not found.`);

		return postComment;
	};

	deleteById = async (id: number): Promise<boolean> => {
		return this.postCommentRepository.deleteById(id);
	};

	getAll = async (
		filters: GetAllPostCommentsFilters
	): Promise<PostCommentOutput[] | undefined> => {
		const postComments = await this.postCommentRepository.getAll(filters);
		if (!postComments) throw new NotFoundError(`Not found.`);
		return postComments;
	};
}
