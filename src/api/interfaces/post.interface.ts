export interface Post {
	id: number;
	title: string;
	content: string;
	user_id?: number;
	description?: string;
	publishedAt?: Date;
	createdAt?: Date;
	updatedAt?: Date;
	deletedAt?: Date;
}
