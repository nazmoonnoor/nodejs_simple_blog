export interface PostComment {
	id: number;
	post_id: number;
	title: string;
	comment: string;
	publishedAt?: Date;
	createdAt?: Date;
	updatedAt?: Date;
	deletedAt?: Date;
}
