import { Post } from "./post.interface";
import { PostComment } from "./post.comment.interface";
import { User } from "./user.interface";

export { Post, PostComment, User };
