import { Request, Response } from "express";
import PostCommentService from "../../services/post.comment.service";
import {
	CreatePostCommentDTO,
	UpdatePostCommentDTO,
	FilterPostCommentsDTO,
} from "../../dto/post.comment.dto";
import * as mapper from "./mapper";
import BaseController from "../base.controller";

export default class PostCommentController extends BaseController {
	constructor(private readonly postCommentService: PostCommentService) {
		super();
		this.postCommentService = postCommentService;
	}

	/** Creates a postcomment
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	create = async (req: Request, res: Response) => {
		const payload: CreatePostCommentDTO = req.body;

		try {
			const response = await this.postCommentService.create(payload);
			if (response) {
				const postComment = mapper.toPostComment(response);
				res.status(this.httpStatusCodes.CREATED).json({ postComment });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Updates a postcomment
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	update = async (req: Request, res: Response) => {
		const id = Number(req.params.id);
		const payload: UpdatePostCommentDTO = req.body;

		try {
			const response = await this.postCommentService.update(id, payload);
			if (response) {
				const postComment = mapper.toPostComment(response);
				res.status(this.httpStatusCodes.OK).json({ postComment });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Gets a postcomment
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	getById = async (req: Request, res: Response) => {
		const id = Number(req.params.id);

		try {
			const response = await this.postCommentService.getById(id);
			if (response) {
				const postComment = mapper.toPostComment(response);
				res.status(this.httpStatusCodes.OK).json({ postComment });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Gets all postcomments
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	getAll = async (req: Request, res: Response) => {
		const filters: FilterPostCommentsDTO = req.query;

		try {
			const response = await this.postCommentService.getAll(filters);
			if (response) {
				const postComments = response.map(mapper.toPostComment);
				res.status(this.httpStatusCodes.OK).json({ postComments });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Deletes a postcomment
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	delete = async (req: Request, res: Response) => {
		const id = Number(req.params.id);

		try {
			const response = await this.postCommentService.deleteById(id);
			if (response) {
				res.status(this.httpStatusCodes.NO_CONTENT).json({ response });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};
}
