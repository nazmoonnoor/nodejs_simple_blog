import { PostComment } from "../../interfaces";
import { PostCommentOutput } from "../../../db/models/post.comment";

export const toPostComment = (postComment: PostCommentOutput): PostComment => {
	return {
		id: postComment.id,
		post_id: postComment.post_id,
		title: postComment.title,
		comment: postComment.comment,
		publishedAt: postComment.publishedAt,
		createdAt: postComment.createdAt,
		updatedAt: postComment.updatedAt,
		deletedAt: postComment.deletedAt,
	};
};
