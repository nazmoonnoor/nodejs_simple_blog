import { Post } from "../../interfaces";
import { PostOutput } from "../../../db/models/post";

export const toPost = (post: PostOutput): Post => {
	return {
		id: post.id,
		title: post.title,
		content: post.content,
		user_id: post.user_id,
		description: post.description,
		publishedAt: post.publishedAt,
		createdAt: post.createdAt,
		updatedAt: post.updatedAt,
		deletedAt: post.deletedAt,
	};
};
