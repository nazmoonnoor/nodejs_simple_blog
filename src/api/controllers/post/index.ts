import { Request, Response } from "express";
import PostService from "../../services/post.service";
import { CreatePostDTO, UpdatePostDTO, FilterPostsDTO } from "../../dto/post.dto";
import * as mapper from "./mapper";
import BaseController from "../base.controller";

export default class PostController extends BaseController {
	constructor(private readonly postService: PostService) {
		super();
		this.postService = postService;
	}

	/** Creates a post
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	create = async (req: Request, res: Response) => {
		const payload: CreatePostDTO = req.body;

		try {
			const response = await this.postService.create(payload);
			if (response) {
				const post = mapper.toPost(response);
				res.status(this.httpStatusCodes.CREATED).json({ post });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Updates a post
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	update = async (req: Request, res: Response) => {
		const id = Number(req.params.id);
		const payload: UpdatePostDTO = req.body;

		try {
			const response = await this.postService.update(id, payload);
			if (response) {
				const post = mapper.toPost(response);
				res.status(this.httpStatusCodes.OK).json({ post });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Gets a post
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	getById = async (req: Request, res: Response) => {
		const id = Number(req.params.id);

		try {
			const response = await this.postService.getById(id);
			if (response) {
				const post = mapper.toPost(response);
				res.status(this.httpStatusCodes.OK).json({ post });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Gets all posts
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	getAll = async (req: Request, res: Response) => {
		const filters: FilterPostsDTO = req.query;

		try {
			const response = await this.postService.getAll(filters);
			if (response) {
				const posts = response.map(mapper.toPost);
				res.status(this.httpStatusCodes.OK).json({ posts });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Deletes a post
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	delete = async (req: Request, res: Response) => {
		const id = Number(req.params.id);

		try {
			const response = await this.postService.deleteById(id);
			if (response) {
				res.status(this.httpStatusCodes.NO_CONTENT).json({ response });
			}
		} catch (err: any) {
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};
}
