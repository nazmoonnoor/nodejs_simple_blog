import { Request, Response } from "express";
import { CreateUserDTO, UpdateUserDTO, FilterUsersDTO } from "../../dto/user.dto";
import * as mapper from "./mapper";
import UserService from "../../services/user.service";
import BaseController from "../base.controller";

export default class UserController extends BaseController {
	constructor(private readonly userService: UserService) {
		super();
		this.userService = userService;
	}

	/** Creates a user
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 * @param  {NextFunction} next	express nextFunction
	 */
	create = async (req: Request, res: Response) => {
		const payload: CreateUserDTO = req.body;

		try {
			const response = await this.userService.create(payload);
			if (response) {
				const user = mapper.toUser(response);
				res.status(this.httpStatusCodes.CREATED).json({ user });
			}
		} catch (err: any) {
			this.logger.error(err);
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Updates a user
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 * @param  {NextFunction} next	express nextFunction
	 */
	update = async (req: Request, res: Response) => {
		const id = Number(req.params.id);
		const payload: UpdateUserDTO = req.body;

		try {
			const response = await this.userService.update(id, payload);
			if (response) {
				const user = mapper.toUser(response);
				res.status(this.httpStatusCodes.OK).json({ user });
			}
		} catch (err: any) {
			this.logger.error(err);
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Gets a user
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 * @param  {NextFunction} next	express nextFunction
	 */
	getById = async (req: Request, res: Response) => {
		const id = Number(req.params.id);

		try {
			const response = await this.userService.getById(id);
			if (response) {
				const user = mapper.toUser(response);
				res.status(this.httpStatusCodes.OK).json({ user });
			}
		} catch (err: any) {
			this.logger.error(err);
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Gets all users
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 * @param  {NextFunction} next	express nextFunction
	 */
	getAll = async (req: Request, res: Response) => {
		const filters: FilterUsersDTO = req.query;

		try {
			const response = await this.userService.getAll(filters);
			if (response) {
				const users = response.map(mapper.toUser);
				res.status(this.httpStatusCodes.OK).json({ users });
			}
		} catch (err: any) {
			this.logger.error(err);
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};

	/** Deletes a user
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 * @param  {NextFunction} next	express nextFunction
	 */
	delete = async (req: Request, res: Response) => {
		const id = Number(req.params.id);

		try {
			const response = await this.userService.deleteById(id);
			if (response) {
				res.status(this.httpStatusCodes.NO_CONTENT).json({ response });
			}
		} catch (err: any) {
			this.logger.error(err);
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};
}
