import axios from "axios";
import { Request, Response } from "express";
import {
	TranslateDTO,
	TranslatePostDTO,
	TranslatePostResponse,
	TranslateResponse,
} from "../../dto/translate.dto";
import TranslationService from "../../services/translation.service";
import BaseController from "../base.controller";
import { BadRequestError } from "../../../utils/errorHandler";

export default class TranslationController extends BaseController {
	translationService: TranslationService;

	constructor() {
		super();
		this.translationService = new TranslationService();
	}

	translate = async (req: Request, res: Response) => {
		try {
			const payloads: TranslateDTO[] = req.body;

			if (!payloads || !Array.isArray(payloads))
				throw new BadRequestError("Inputs were not correct");

			const translations: TranslateResponse[] = [];
			const promises = await payloads.map(async (data) => {
				const translatedText = await this.translationService.getTranslation(data);
				translations.push({
					sourceLang: data.sourceLang,
					targetLang: data.targetLang,
					text: data.text,
					translatedText,
				});
			});

			await Promise.all(promises);

			res.status(this.httpStatusCodes.OK).json({ translations });
		} catch (err: any) {
			this.logger.error(err);
			res.status((err && err.statusCode) || this.httpStatusCodes.INTERNAL_SERVER).json({
				err,
			});
		}
	};
}
