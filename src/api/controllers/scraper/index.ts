import { Request, Response } from "express";
import ScraperBrowser from "../../services/scraper.service/browser";
import { ScraperPostDTO } from "../../dto/scrap.dto";
import { CreatePostDTO } from "../../dto/post.dto";
import { BadRequestError, ApiInternalError } from "../../../utils/errorHandler";
import BaseController from "../base.controller";
import ScraperService from "../../services/scraper.service";

export default class ScraperController extends BaseController {
	constructor(private readonly scraperBrowser: ScraperBrowser) {
		super();
	}

	/** Scrap a post
	 * @param  {Request} req	express request parameter
	 * @param  {Response} res	express response parameter
	 */
	scrap = async (req: Request, res: Response) => {
		try {
			const payloads: ScraperPostDTO[] = req.body;
			if (!payloads || !Array.isArray(payloads))
				throw new BadRequestError("Inputs were not correct");

			const browser = await this.scraperBrowser.startBrowser();
			if (!browser) throw new ApiInternalError("Puppeteer browser could not be loaded!");

			const scraperService = new ScraperService(browser);
			const posts: CreatePostDTO[] = [];

			const promisePosts = await payloads.map(async (data) => {
				const { url } = data;
				const scraped = await scraperService.scrapPage({ browser, url });
				if (scraped) posts.push(scraped);
			});

			await Promise.all(promisePosts);

			await browser.close();
			res.status(200).json({ posts });
		} catch (err) {
			console.log("Could not resolve the browser instance => ", err);
		}
	};
}
