import { Router } from "express";
import userRouter from "./user";
import postRouter from "./post";
import postCommentRouter from "./post.comment";
import scraperRouter from "./scraper";
import translationRouter from "./translate";

const router = Router();

router.use("/users", userRouter);
router.use("/posts", postRouter);
router.use("/post-comments", postCommentRouter);
router.use("/scrap", scraperRouter);
router.use("/translate", translationRouter);

export default router;
