import { Router } from "express";
import { ValidateJoi } from "../../middleware/joi";
import UserController from "../controllers/user";
import { Validator } from "../validations/user.validation";
import UserRepository from "../../db/repository/user.repository";
import UserService from "../services/user.service";

const userRouter = Router();

const userRepository = new UserRepository();
const userService = new UserService(userRepository);
const userController = new UserController(userService);

userRouter.post("/", ValidateJoi(Validator.user.create), userController.create);
userRouter.patch("/:id", ValidateJoi(Validator.user.update), userController.update);
userRouter.get("/:id", userController.getById);
userRouter.get("/", userController.getAll);
userRouter.delete("/:id", userController.delete);

export default userRouter;
