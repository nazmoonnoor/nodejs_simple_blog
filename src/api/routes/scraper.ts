import { Router } from "express";
import ScraperController from "../controllers/scraper";
import ScraperBrowser from "../services/scraper.service/browser";

const scraperRouter = Router();

const browser = new ScraperBrowser();
const scraperController = new ScraperController(browser);

scraperRouter.post("/", scraperController.scrap);

export default scraperRouter;
