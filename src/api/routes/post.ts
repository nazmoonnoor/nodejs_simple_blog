import { Router } from "express";
import { ValidateJoi } from "../../middleware/joi";
import PostController from "../controllers/post";
import { Validator } from "../validations/post.validation";
import PostRepository from "../../db/repository/post.repository";
import PostService from "../services/post.service";

const postRouter = Router();

const postRepository = new PostRepository();
const postService = new PostService(postRepository);
const postController = new PostController(postService);

postRouter.post("/", ValidateJoi(Validator.post.create), postController.create);
postRouter.patch("/:id", ValidateJoi(Validator.post.update), postController.update);
postRouter.get("/:id", postController.getById);
postRouter.get("/", postController.getAll);
postRouter.delete("/:id", postController.delete);

export default postRouter;
