import { Router } from "express";
import TranslationController from "../controllers/translation";

const translationRouter = Router();

const translationController = new TranslationController();

translationRouter.post("/", translationController.translate);

export default translationRouter;
