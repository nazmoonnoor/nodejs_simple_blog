import { Router } from "express";
import { ValidateJoi } from "../../middleware/joi";
import PostCommentController from "../controllers/postcomment";
import { Validator } from "../validations/post.comment.validation";
import PostCommentRepository from "../../db/repository/post.comment.repository";
import PostCommentService from "../services/post.comment.service";

const postCommentRouter = Router();

const postCommentRepository = new PostCommentRepository();
const postCommentService = new PostCommentService(postCommentRepository);
const postCommentController = new PostCommentController(postCommentService);

postCommentRouter.post(
	"/",
	ValidateJoi(Validator.postComment.create),
	postCommentController.create
);
postCommentRouter.patch(
	"/:id",
	ValidateJoi(Validator.postComment.update),
	postCommentController.update
);
postCommentRouter.get("/:id", postCommentController.getById);
postCommentRouter.get("/", postCommentController.getAll);
postCommentRouter.delete("/:id", postCommentController.delete);

export default postCommentRouter;
