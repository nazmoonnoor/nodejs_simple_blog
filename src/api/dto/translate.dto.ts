export type TranslateDTO = {
	sourceLang: string;
	targetLang: string;
	text: string;
};

export type TranslateResponse = TranslateDTO & { translatedText: string };

export type TranslatePostDTO = {
	sourceLang: string;
	targetLang: string;
	title: string;
	content: string;
};

export type TranslatePostResponse = TranslatePostDTO & {
	translatedTitle: string;
	translatedContent: string;
};
