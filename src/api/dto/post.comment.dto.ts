import { Optional } from "sequelize/types";

export type CreatePostCommentDTO = {
	post_id: number;
	title: string;
	comment: string;
	publishedAt?: Date;
};

export type UpdatePostCommentDTO = Optional<CreatePostCommentDTO, "title">;

export type FilterPostCommentsDTO = {
	isDeleted?: boolean;
	includeDeleted?: boolean;
};
