import { Optional } from "sequelize/types";

export type CreatePostDTO = {
	title: string;
	content: string;
	user_id?: number;
	description?: string;
	publishedAt?: Date;
};

export type UpdatePostDTO = Optional<CreatePostDTO, "title">;

export type FilterPostsDTO = {
	isDeleted?: boolean;
	includeDeleted?: boolean;
};
