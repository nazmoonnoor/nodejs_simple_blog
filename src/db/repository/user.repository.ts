import { Op } from "sequelize";
import { isEmpty } from "lodash";
import { DbOperationError } from "../../utils/errorHandler";

import { User } from "../models";
import { GetAllUsersFilters } from "./types";
import { UserInput, UserOutput } from "../models/user";

export default class UserRepository {
	create = async (payload: UserInput): Promise<UserOutput> => {
		try {
			return await User.create(payload);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	findOrCreate = async (payload: UserInput): Promise<UserOutput> => {
		try {
			const [user] = await User.findOrCreate({
				where: {
					name: payload.name,
				},
				defaults: payload,
			});

			return user;
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	update = async (id: number, payload: Partial<UserInput>): Promise<UserOutput | null> => {
		const user = await User.findByPk(id);

		if (!user) {
			return user;
		}
		try {
			return await user.update(payload);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	getById = async (id: number): Promise<UserOutput | null> => {
		try {
			return await User.findByPk(id);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	deleteById = async (id: number): Promise<boolean> => {
		try {
			const deletedUserCount = await User.destroy({
				where: { id },
			});

			return !!deletedUserCount;
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	getAll = async (filters?: GetAllUsersFilters): Promise<UserOutput[]> => {
		try {
			return await User.findAll({
				where: {
					...(filters?.isDeleted && { deletedAt: { [Op.not]: null } }),
				},
				...((filters?.isDeleted || filters?.includeDeleted) && { paranoid: true }),
			});
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};
}
