import { Op } from "sequelize";
import { DbOperationError } from "../../utils/errorHandler";

import { PostComment } from "../models";
import { GetAllPostCommentsFilters } from "./types";
import { PostCommentInput, PostCommentOutput } from "../models/post.comment";

export default class PostCommentRepository {
	create = async (payload: PostCommentInput): Promise<PostCommentOutput> => {
		try {
			return await PostComment.create(payload);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	findOrCreate = async (payload: PostCommentInput): Promise<PostCommentOutput> => {
		try {
			const [postComment] = await PostComment.findOrCreate({
				where: {
					title: payload.title,
				},
				defaults: payload,
			});

			return postComment;
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	update = async (
		id: number,
		payload: Partial<PostCommentInput>
	): Promise<PostCommentOutput | null> => {
		const postComment = await PostComment.findByPk(id);

		if (!postComment) {
			return postComment;
		}
		try {
			return await postComment.update(payload);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	getById = async (id: number): Promise<PostCommentOutput | null> => {
		try {
			return await PostComment.findByPk(id);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	deleteById = async (id: number): Promise<boolean> => {
		try {
			const deletedPostCommentCount = await PostComment.destroy({
				where: { id },
			});

			return !!deletedPostCommentCount;
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	getAll = async (filters?: GetAllPostCommentsFilters): Promise<PostCommentOutput[]> => {
		try {
			return await PostComment.findAll({
				where: {
					...(filters?.isDeleted && { deletedAt: { [Op.not]: null } }),
				},
				...((filters?.isDeleted || filters?.includeDeleted) && { paranoid: true }),
			});
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};
}
