interface ListFilters {
	isDeleted?: boolean;
	includeDeleted?: boolean;
}

export type GetAllUsersFilters = ListFilters;
export type GetAllPostsFilters = ListFilters;
export type GetAllPostCommentsFilters = ListFilters;
export type GetAllTagsFilters = ListFilters;
export type GetAllCategorysFilters = ListFilters;
