import { Op } from "sequelize";
import { DbOperationError } from "../../utils/errorHandler";

import { Post } from "../models";
import { GetAllPostsFilters } from "./types";
import { PostInput, PostOutput } from "../models/post";

export default class PostRepository {
	create = async (payload: PostInput): Promise<PostOutput> => {
		try {
			return await Post.create(payload);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	findOrCreate = async (payload: PostInput): Promise<PostOutput> => {
		try {
			const [post] = await Post.findOrCreate({
				where: {
					title: payload.title,
				},
				defaults: payload,
			});

			return post;
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	update = async (id: number, payload: Partial<PostInput>): Promise<PostOutput | null> => {
		const post = await Post.findByPk(id);

		if (!post) {
			return post;
		}
		try {
			return await post.update(payload);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	getById = async (id: number): Promise<PostOutput | null> => {
		try {
			return await Post.findByPk(id);
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	deleteById = async (id: number): Promise<boolean> => {
		try {
			const deletedPostCount = await Post.destroy({
				where: { id },
			});

			return !!deletedPostCount;
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};

	getAll = async (filters?: GetAllPostsFilters): Promise<PostOutput[]> => {
		try {
			return await Post.findAll({
				where: {
					...(filters?.isDeleted && { deletedAt: { [Op.not]: null } }),
				},
				...((filters?.isDeleted || filters?.includeDeleted) && { paranoid: true }),
			});
		} catch (err: any) {
			throw new DbOperationError(`${err.name}: ${err.message}`);
		}
	};
}
