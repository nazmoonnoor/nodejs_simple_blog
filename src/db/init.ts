import dotenv from "dotenv";
import seed from "./seed";

import { Post, User, PostComment } from "./models";

dotenv.config();

const isDev = process.env.NODE_ENV === "development";
const isTest = process.env.NODE_ENV !== "test";

const dbInit = () =>
	Promise.all([User.sync({ alter: isDev || isTest })])
		.then(() => {
			Post.sync({ alter: isDev || isTest });
			return true;
		})
		.then(() => {
			PostComment.sync({ alter: isDev || isTest });
			return seed();
		});

export default dbInit;
