import { DataTypes, Model, Optional } from "sequelize";
import sequelizeConnection from "../config";
import User from "./user";

interface UserGroupAttributes {
	id: number;
	name: string;
	users: User[];
	description?: string;
	createdAt?: Date;
	updatedAt?: Date;
	deletedAt?: Date;
}

export type UserGroupInput = Optional<UserGroupAttributes, "id">;

export type UserGroupOuput = Required<UserGroupAttributes>;

class UserGroup extends Model implements UserGroupAttributes {
	public id!: number;

	public name!: string;

	public users!: User[];

	public description!: string;

	// timestamps!
	public readonly createdAt!: Date;

	public readonly updatedAt!: Date;

	public readonly deletedAt!: Date;
}

UserGroup.init(
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		users: {
			type: DataTypes.ARRAY,
			allowNull: true,
		},
		description: {
			type: DataTypes.TEXT,
		},
	},
	{
		sequelize: sequelizeConnection,
		paranoid: true,
		underscored: true,
		schema: "kotha",
	}
);

export default UserGroup;
