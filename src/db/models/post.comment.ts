import { DataTypes, Model, Optional } from "sequelize";
import sequelizeConnection from "../config";
import Post from "./post";

interface PostCommentAttributes {
	id: number;
	post_id: number;
	title: string;
	comment: string;
	publishedAt?: Date;
	createdAt?: Date;
	updatedAt?: Date;
	deletedAt?: Date;
}

export type PostCommentInput = Optional<PostCommentAttributes, "id">;

export type PostCommentOutput = Required<PostCommentAttributes>;

class PostComment
	extends Model<PostCommentAttributes, PostCommentInput>
	implements PostCommentAttributes
{
	public id!: number;

	public post_id!: number;

	public title!: string;

	public comment!: string;

	public publishedAt!: Date;

	// timestamps!
	public readonly createdAt!: Date;

	public readonly updatedAt!: Date;

	public readonly deletedAt!: Date;
}

PostComment.init(
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		post_id: {
			type: DataTypes.INTEGER,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		comment: {
			type: DataTypes.TEXT,
			allowNull: false,
		},
		publishedAt: {
			type: DataTypes.DATE,
		},
	},
	{
		sequelize: sequelizeConnection,
		paranoid: true,
		underscored: true,
		schema: "kotha",
	}
);

// Post & PostComment
Post.hasMany(PostComment, {
	foreignKey: "post_id",
});

PostComment.belongsTo(Post, {
	foreignKey: "post_id",
});

export default PostComment;
