import { DataTypes, Model, Optional } from "sequelize";
import sequelizeConnection from "../config";
import User from "./user";

interface PostAttributes {
	id: number;
	title: string;
	content: string;
	user_id: number;
	description?: string;
	publishedAt?: Date;
	createdAt?: Date;
	updatedAt?: Date;
	deletedAt?: Date;
}

export type PostInput = Optional<PostAttributes, "id">;

export type PostOutput = Required<PostAttributes>;

class Post extends Model<PostAttributes, PostInput> implements PostAttributes {
	public id!: number;

	public title!: string;

	public content!: string;

	public user_id!: number;

	public description!: string;

	public publishedAt!: Date;

	// timestamps!
	public readonly createdAt!: Date;

	public readonly updatedAt!: Date;

	public readonly deletedAt!: Date;
}

Post.init(
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		content: {
			type: DataTypes.TEXT,
			allowNull: false,
		},
		user_id: {
			type: DataTypes.INTEGER,
		},
		description: {
			type: DataTypes.TEXT,
		},
		publishedAt: {
			type: DataTypes.DATE,
		},
	},
	{
		sequelize: sequelizeConnection,
		paranoid: true,
		underscored: true,
		schema: "kotha",
	}
);

// User & Post
User.hasMany(Post, {
	foreignKey: "user_id",
});

Post.belongsTo(User, {
	foreignKey: "user_id",
});

export default Post;
