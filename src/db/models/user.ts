import { DataTypes, Model, Optional } from "sequelize";
import sequelizeConnection from "../config";

interface UserAttributes {
	id: number;
	name: string;
	email: string;
	createdAt?: Date;
	updatedAt?: Date;
	deletedAt?: Date;
}

export type UserInput = Optional<UserAttributes, "id" | "email">;

export type UserOutput = Required<UserAttributes>;

class User extends Model<UserAttributes, UserInput> implements UserAttributes {
	public id!: number;

	public name!: string;

	public email!: string;

	// timestamps!
	public readonly createdAt!: Date;

	public readonly updatedAt!: Date;

	public readonly deletedAt!: Date;
}

User.init(
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		email: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true,
		},
	},
	{
		sequelize: sequelizeConnection,
		paranoid: true,
		underscored: true,
		schema: "kotha",
	}
);

export default User;
