import { Post, PostComment } from "./models";
import User from "./models/user";

const getRandom = (min: any, max: any): number => {
	return Math.floor(Math.random() * (max - min)) + min;
};

const users = [
	{
		email: "kbodicum0@blogtalkradio.com",
		name: "Thérèsa",
	},
	{
		email: "phubane1@parallels.com",
		name: "Maïté",
	},
	{
		email: "rghidotti2@yahoo.co.jp",
		name: "Andréa",
	},
	{
		email: "vninnis3@yahoo.co.jp",
		name: "Séverine",
	},
	{
		email: "kzaniolini4@economist.com",
		name: "Dafnée",
	},
	{
		email: "phuske5@wunderground.com",
		name: "Michèle",
	},
	{
		email: "pstiegar6@cyberchimps.com",
		name: "Réservés",
	},
	{
		email: "ybasant7@posterous.com",
		name: "Lèi",
	},
	{
		email: "bgoodboddy8@statcounter.com",
		name: "Göran",
	},
	{
		email: "mtampen9@symantec.com",
		name: "Uò",
	},
	{
		email: "gphilotta@hibu.com",
		name: "Pål",
	},
	{
		email: "fvaadelandb@elegantthemes.com",
		name: "Anaël",
	},
	{
		email: "jbirchenoughc@friendfeed.com",
		name: "Laurélie",
	},
	{
		email: "whalliwelld@amazon.co.uk",
		name: "Hélène",
	},
	{
		email: "etophame@usatoday.com",
		name: "Régine",
	},
	{
		email: "aoxbrowf@last.fm",
		name: "Dà",
	},
	{
		email: "coagg@dropbox.com",
		name: "Marlène",
	},
	{
		email: "cduesburyh@liveinternet.ru",
		name: "Danièle",
	},
	{
		email: "ipetraccoi@clickbank.net",
		name: "Marie-thérèse",
	},
	{
		email: "sbenechj@huffingtonpost.com",
		name: "Léone",
	},
	{
		email: "msymcockk@stumbleupon.com",
		name: "Loïca",
	},
	{
		email: "dbrundalel@com.com",
		name: "Simplifiés",
	},
	{
		email: "gcarnockm@eventbrite.com",
		name: "Clémence",
	},
	{
		email: "lartzn@reddit.com",
		name: "Renée",
	},
	{
		email: "tdezamorao@prlog.org",
		name: "Cécilia",
	},
];

const posts = [
	{
		title: "Nulla eu dui",
		content:
			"volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor",
	},
	{
		title: "Duis eu dui",
		content:
			"a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna",
	},
	{
		title: "Quisque eu dui",
		content:
			"lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at,",
	},
	{
		title: "Mauris eu dui",
		content:
			"mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu",
	},
	{
		title: "Nunc eu dui",
		content:
			"vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut,",
	},
	{
		title: "Quisque eu dui",
		content:
			"risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque",
	},
	{
		title: "Praesent eu dui",
		content: "at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis",
	},
];

const postcomments = [
	{
		title: "Nulla eu dui",
		comment: "purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum",
	},
	{
		title: "Nulla eu dui",
		comment:
			"egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque",
	},
	{
		title: "Nulla eu dui",
		comment:
			"risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus",
	},
	{
		title: "Nulla eu dui",
		comment: "pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat,",
	},
	{
		title: "Nulla eu dui",
		comment:
			"consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis,",
	},
	{
		title: "Nulla eu dui",
		comment:
			"sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus.",
	},
	{
		title: "Nulla eu dui",
		comment:
			"malesuada. Integer id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros",
	},
	{
		title: "Nulla eu dui",
		comment:
			"Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit",
	},
	{
		title: "Nulla eu dui",
		comment: "Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend",
	},
	{
		title: "Nulla eu dui",
		comment: "sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor",
	},
	{
		title: "Nulla eu dui",
		comment:
			"gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla",
	},
	{
		title: "Nulla eu dui",
		comment:
			"taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien",
	},
	{
		title: "Nulla eu dui",
		comment:
			"vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede",
	},
	{
		title: "Nulla eu dui",
		comment: "Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede.",
	},
	{
		title: "Nulla eu dui",
		comment:
			"nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean",
	},
	{
		title: "Nulla eu dui",
		comment: "egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis. Cras eget",
	},
	{
		title: "Nulla eu dui",
		comment:
			"purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante",
	},
	{
		title: "Nulla eu dui",
		comment:
			"amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl",
	},
	{
		title: "Nulla eu dui",
		comment:
			"tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit",
	},
	{
		title: "Nulla eu dui",
		comment:
			"blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas",
	},
];
const seed = async () => {
	const user = await User.findOne({
		where: {
			email: "kbodicum0@blogtalkradio.com",
		},
	});

	// Seeds users
	if (!user) {
		for (let i = 0; i < users.length; i++) {
			User.create({ name: users[i].name, email: users[i].email });
		}
	}

	const post = await Post.findOne({
		where: {
			id: 1,
		},
	});
	// Seeds posts
	if (!post) {
		for (let i = 0; i < posts.length; i++) {
			Post.create({
				title: posts[i].title,
				content: posts[i].content,
			});
		}
	}

	const postComment = await PostComment.findOne({
		where: {
			id: 1,
		},
	});

	// Seeds post-comments
	// if (!postComment) {
	// 	for (let i = 0; i < postcomments.length; i++) {
	// 		PostComment.create({
	// 			title: postcomments[i].title,
	// 			comment: postcomments[i].comment,
	// 			post_id: getRandom(1, 10),
	// 		});
	// 	}
	// }
};

export default seed;
