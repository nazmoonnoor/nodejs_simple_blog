import app from "./app";
import dotenv from "dotenv";

dotenv.config();
const port = process.env.PORT || 1337;

const a = app.listen(port, () => {
	console.log(`We have been launched on port: ${port}.`);
});

export default a;
