import app from "../app";
import supertest from "supertest";
const request = supertest(app);

describe("POST / create post endpoints ", () => {
	it("The endpoint returns an error because data doesn't exist", async () => {
		const result = await request.post("/api/v1/posts").send({});

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint returns an error because of data is empty", async () => {
		const result = await request.post("/api/v1/posts").send({ data: [] });

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint returns `created`::201", async () => {
		const result = await request.post("/api/v1/posts").send({
			title: "Test title",
			content: "Ich gehe zum Supermarkt.",
		});
		expect(result.statusCode).toEqual(201);
	});
});

describe("PATCH / update post endpoints ", () => {
	it("The endpoint returns an error because of data is in wrong format", async () => {
		const result = await request.patch("/api/v1/posts/1").send({ data: [] });

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint should returns okay result, even when not data to update", async () => {
		const result = await request.patch("/api/v1/posts/1").send({});

		expect(result.statusCode).toEqual(200);
	});

	it("The endpoint returns `updated`::200", async () => {
		const result = await request.patch("/api/v1/posts/1").send({
			title: "Test title",
			content: "Ich gehe zum Supermarkt.",
		});
		expect(result.statusCode).toEqual(200);
	});
});

describe("GET / gets all post endpoint", () => {
	it("The endpoint returns all posts", async () => {
		const result = await request.get("/api/v1/posts");
		expect(result.body.posts.length).toBeGreaterThan(0);
		expect(result.statusCode).toEqual(200);
	});
});

describe("GET / gets by post_id endpoint ", () => {
	it("The endpoint returns post by id", async () => {
		const result = await request.get("/api/v1/posts/1");
		expect(result.body.post).toBeTruthy();
		expect(result.statusCode).toEqual(200);
	});
});
