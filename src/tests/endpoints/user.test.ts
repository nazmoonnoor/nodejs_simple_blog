import app from "../app";
import supertest from "supertest";
import crypto from "crypto";
const request = supertest(app);

describe("POST / create user endpoints ", () => {
	it("The endpoint returns an error because data doesn't exist", async () => {
		const result = await request.post("/api/v1/users").send({});

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint returns an error because of data is empty", async () => {
		const result = await request.post("/api/v1/users").send({ data: [] });

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint returns `created`::201", async () => {
		const uuid = crypto.randomUUID();
		const result = await request.post("/api/v1/users").send({
			email: `${uuid}@email.com`,
			name: "Test Name",
		});
		expect(result.statusCode).toEqual(201);
	});
});

describe("PATCH / update user endpoints ", () => {
	it("The endpoint returns an error because of data is in wrong format", async () => {
		const result = await request.patch("/api/v1/users/1").send({ data: [] });

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint should returns okay result, even when not data to update", async () => {
		const result = await request.patch("/api/v1/users/1").send({});

		expect(result.statusCode).toEqual(200);
	});

	it("The endpoint returns `updated`::200", async () => {
		const result = await request.patch("/api/v1/users/1").send({
			name: "Test Name",
		});
		expect(result.statusCode).toEqual(200);
	});
});

describe("GET / gets all user endpoint", () => {
	it("The endpoint returns all users", async () => {
		const result = await request.get("/api/v1/users");
		expect(result.body.users.length).toBeGreaterThan(0);
		expect(result.statusCode).toEqual(200);
	});
});

describe("GET / gets by user_id endpoint ", () => {
	it("The endpoint returns user by id", async () => {
		const result = await request.get("/api/v1/users/1");
		expect(result.body.user).toBeTruthy();
		expect(result.statusCode).toEqual(200);
	});
});
