import app from "../app";
import supertest from "supertest";
const request = supertest(app);
const ID = 2;

describe("POST / create postcomment endpoints ", () => {
	it("The endpoint returns an error because data doesn't exist", async () => {
		const result = await request.post("/api/v1/post-comments").send({});

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint returns an error because of data is empty", async () => {
		const result = await request.post("/api/v1/post-comments").send({ data: [] });

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint returns `created`::201", async () => {
		const result = await request.post("/api/v1/post-comments").send({
			post_id: 1,
			title: "Test title",
			comment: "Test comment",
		});
		expect(result.statusCode).toEqual(201);
	});
});

describe("PATCH / update postcomment endpoints ", () => {
	it("The endpoint returns an error because of data is in wrong format", async () => {
		const result = await request.patch(`/api/v1/post-comments/${ID}`).send({ data: [] });

		expect(result.statusCode).toEqual(422);
	});

	it("The endpoint should returns okay result, even when not data to update", async () => {
		const result = await request.patch(`/api/v1/post-comments/${ID}`).send({});

		expect(result.statusCode).toEqual(200);
	});

	it("The endpoint returns `updated`::200", async () => {
		const result = await request.patch(`/api/v1/post-comments/${ID}`).send({
			comment: "Test comment",
		});
		expect(result.statusCode).toEqual(200);
	});
});

describe("GET / gets all postcomment endpoint ", () => {
	it("The endpoint returns all postcomments", async () => {
		const result = await request.get("/api/v1/post-comments");

		expect(result.body.postComments.length).toBeGreaterThan(0);
		expect(result.statusCode).toEqual(200);
	});
});

describe("GET / gets by comment_id endpoint ", () => {
	it("The endpoint returns postcomment by id", async () => {
		const result = await request.get(`/api/v1/post-comments/${ID}`);
		expect(result.body.postComment).toBeTruthy();
		expect(result.statusCode).toEqual(200);
	});
});
