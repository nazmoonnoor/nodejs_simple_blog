import express from "express";
import dbInit from "../db/init";
import routes from "../api/routes";

const app = express();

dbInit();

app.use(express.json());

app.use("/api/v1", routes);

export default app;
