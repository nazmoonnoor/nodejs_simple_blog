# Simple Blog API

The API project build with Node.js and Typescript.
Runtime and Tools that are used:

-   [Express.js](https://expressjs.com/)
-   [Typescript](https://gitlab.com/nazmoonnoor/nodejs_simple_blog)
-   [Sequelize](https://sequelize.org/)
-   [Puppeteer](https://pptr.dev/)

## Clone the project

```bash
  git clone https://gitlab.com/nazmoonnoor/nodejs_simple_blog.git
```

```bash
  cd nodejs_simple_blog
```

## Run Locally

Go to the project directory

```bash
  cd nodejs_simple_blog
```

Environment file

```bash
   Rename `.env.example` file to `.env`.
```

Create db

```bash
Install postgres and create `kothadb` and `kothadb_test`, check `.env` file db-connection info.
```

Install

```bash
  yarn install
```

Run

```bash
  yarn run dev
```

## Run use cases with Postman collection

-   Postman collection and environment json has been shared in root folder. Please download them and import to postman.

## API Reference

#### Users

```http
  GET /api/v1/users
```

#### Posts

```http
  GET /api/v1/posts
```

#### Post Comments

```http
  GET /api/v1/post-comments
```

#### Web scraper

```http
  POST /api/v1/scraper
```

#### Translation API

```http
  POST /api/v1/translate
```
